// Home Route

Router.route('/', function () {
  Router.go('/logworkout');
});

Router.route('/logworkout', function () {
  this.render('home');
  SEO.set({ title: 'Home' });
});

Router.route('/manage', function () {
  Router.go('/manage/bodypart');
});

//TODO: Check for the parent child relationship while forming routes
Router.route('/manage/bodypart', function (argument) {
  this.render('/bodypart');
  SEO.set({ title: 'Manage: Body Part' });
});

Router.route('/manage/workout', function () {
  this.render('/workout');
  SEO.set({ title: 'Manage: Workout' });
})