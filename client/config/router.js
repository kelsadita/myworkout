Router.configure({
  layoutTemplate: 'basicLayout',
  notFoundTemplate: 'notFound',

  // wait on the following subscriptions before rendering the page to ensure
  // the data it's expecting is present
  waitOn: function() {
    return [
      Meteor.subscribe('BodyPart'),
      Meteor.subscribe('Workout')
    ];
  }
});