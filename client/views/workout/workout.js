/**
*
* Add Workout form template
*
**/

Template.addWorkoutForm.helpers({
  bodyParts: function () {
    return BodyPartService.getBodyPartList();
  }
});

Template.addWorkoutForm.events({

  'click #addMeasuredIn': function(event) {

    event.preventDefault();

    // Get the selected measured in option
    var selectedMeasuredInValue = $('#measuredIn option:selected').val();
    
    console.log(selectedMeasuredInValue);
    // Check if the default option is selected
    if (selectedMeasuredInValue === "deafault-measure-in") {
      return false;
    }

    // Adding selected measured in 
    var measuredInRemoveIcon = '<span class="vcenter cursor-pointer glyphicon glyphicon-remove measured-in-remove" aria-hidden="true"></span>';
    $('#selectedMeasuredIn').append('<h3><span class="label label-info selected-measured-ins" data-selected-measured-in="' + selectedMeasuredInValue + '">' + selectedMeasuredInValue + '&nbsp;' + measuredInRemoveIcon + '</span></h3>');

    // Disabling selected measured in from drop down
    $('#measuredIn').find(':selected').prop('disabled', true);

    // Reset to the default->first option tag
    $("#measuredIn").val($("#measuredIn option:first").val());

  },

  'click .measured-in-remove': function (event) {

    // Re-enabling the deleted measured in from drop down
    var selectedMeasuredIn = $(event.target).parent().data('selected-measured-in');
    $('option[value="' + selectedMeasuredIn + '"]').prop('disabled', false);

    // Remove the HTML of selected measured in which is removed
    $(event.target).closest('h3').remove();
  }
});

// TODO: Check clean alternative for form validation
Template.addWorkoutForm.isInputValid = function() {
  return !($('#workoutName').val() === undefined || 
    $('#bodyPart').val() === 'default-body-part' || 
    $('.selected-measured-ins').length === 0);
}

/**
*
* Add new workout modal
*
**/
Template.addWorkoutModal.resetAddWorkoutForm = function (){
  // resetAddWorkoutForm: function() {
    $("#workoutName").val('');
    $("#bodyPart").val($("#bodyPart option:first").val());
    $("#measuredIn").val($("#measuredIn option:first").val());
    $('.selected-measured-ins').each(function (index, measuredInItem) {
      $(measuredInItem).find('.measured-in-remove').click();
    });
  // }
};

Template.addWorkoutModal.events({
  'click #addNewWorkoutClose': function() {

    // Reset the modal to nothing once closed.
    Template.addWorkoutModal.resetAddWorkoutForm();
  },

  'click #addWorkout': function (event) {
    event.preventDefault();
    var workoutName = $('#workoutName').val();
    var bodyPartId = $('#bodyPart').val();

    if (!Template.addWorkoutForm.isInputValid()) {
      console.log('form is not valid');
      return false;
    }

    $('.selected-measured-ins').each(function (index, selected) {
      // body...
    })
  }
});
