Template.header.created = function () {
  Session.set('isActive', false);
  Session.set('showLogin', false);
};

Template['header'].helpers({
  currentPageIs: function (currentPageRoute) {
    return Router.current().route.getName().contains( currentPageRoute ) ? 'active' : '';
  }
});

Template['header'].events({

});

