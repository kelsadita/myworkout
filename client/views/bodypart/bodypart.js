// Helpers
Template.bodypart.helpers({
  bodyParts: function () {
    return BodyPartService.getBodyPartList();
  }
});

// Events
Template.bodypart.events({

  'keypress #bodyPartInput': function (event) {
    if(event.keyCode == 13) {
      event.preventDefault();
      $("#addBodyPart").click();
    }
  },

  'click #addBodyPart': function (event) {
    var bodyPartName = $('#bodyPartInput').val() ;
    BodyPartService.addBodyPart(bodyPartName);
    $('#bodyPartInput').val('');
  }
});

Template.bodyPartItem.events({

  'click #deleteBodyPart': function (event) {
    event.preventDefault();
    BodyPartService.removeBodyPart(this._id);
  },

  'click #editBodyPart': function (event) {
    $('#editModalLabel').text('Edit: ' + this.name);
    $('#editBodyPartInput').val(this.name);
    $('#saveEditedBodyPart').data('bodypart-id', this._id);
    console.log($('#saveEditedBodyPart').data('bodypart-id'));
  },

});

Template.editBodyPartItem.events({
  'click #saveEditedBodyPart': function (event) {
    event.preventDefault();
    var bodyPartId = $('#saveEditedBodyPart').data('bodypart-id');
    var updatedBodyPartName = $('#editBodyPartInput').val();
    BodyPartService.updateBodyPart(bodyPartId, updatedBodyPartName);
    $('#editBodyPartModal').modal('toggle');
  }
});

// Services:
BodyPartService = {
 
  getBodyPartList: function () {
    return BodyPart.find({}, {sort: {name: 1}});
  },

  addBodyPart: function (bodyPartName) {
    Meteor.call('addBodyParts', bodyPartName, function (err, message) {
      
    });
  },

  removeBodyPart: function (bodyPartId) {
    BodyPart.remove(bodyPartId);
  },

  updateBodyPart: function (bodyPartId, updatedBodyPartName) {
    BodyPart.update(bodyPartId, {$set: {name: updatedBodyPartName}});
  }

};