describe("Body Parts Module Service Testing: ", function() {
  it("should ask for all body parts in ascending order", function() {
    var result = {};
    spyOn(BodyPart, 'find').and.returnValue(result);


    expect(BodyPartService.getBodyPartList()).toBe(result);
    expect(BodyPart.find.calls.argsFor(0)).toEqual([{}, {sort: {name: 1}}]);
  });

});
