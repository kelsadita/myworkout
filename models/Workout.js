Workout = new Mongo.Collection('Workout');

Workout.attachSchema(
  new SimpleSchema({
    name: {
      type: String
    },
    bodyPart: {
      type: [BodyPart]
    },
    measuredIn: {
      type: [String]
    },
    createdAt: {
      type: Date,
      denyUpdate: true
    }
  })
);

if (Meteor.isServer) {
  Workout.allow({
    insert : function () {
      return true;
    },
    update : function () {
      return true;
    },
    remove : function () {
      return true;
    }
  });
}