Meteor.startup(function () {
  Meteor.methods({
    addBodyParts: function (bodyPartName) {
      check(bodyPartName, String);
      BodyPart.insert({
        name: bodyPartName,
        createdAt: new Date()
      });
    }
  });
});